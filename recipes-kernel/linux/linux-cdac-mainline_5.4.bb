require recipes-kernel/linux/linux-cdac-mainline-common.inc

LINUX_VERSION ?= "5.4.x"
KERNEL_VERSION_SANITY_SKIP="1"

BRANCH = "linux-5.4.y"
#SRCREV = "${AUTOREV}"
SRC_URI = "git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git;name=kernel;branch=${BRANCH} \
#		file://Supppor_Eth_and_Plic.patch \
		file://0003-remove-hardcoded-cmdline.patch \
		"

SRCREV_kernel = "52f6ded2a377ac4f191c84182488e454b1386239"

PV = "${LINUX_VERSION}+git${SRCPV}"
FILEEXTRAPATHS_append="${WORKDIR}/files/cdac-vega"
FILEEXTRAPATHS_append="${WORKDIR}/files/cdac-ooo"
#KBUILD_DEFCONFIG_cdac-main ="defconfig"

COMPATIBLE_MACHINE = "cdac-ooo|cdac-vega"

SRC_URI += "file://cdac-ooo-defconfig"

#S = "${WORKDIR}/5.4.x+gitAUTOINC+bdc3a8f6a8-r0"

