# meta-cdac Layer

It's a minimal [meta-cdac](https://gitlab.com/cdachyd/mdp/meta-cdac) layer on top of [meta-riscv](https://github.com/riscv/meta-riscv) to provide additional modifications and new disk image targets.

For advanced OE usage we advice to look into the following third-party manuals:

- [BitBake User Manual 3.1.2 by Yocto](https://www.yoctoproject.org/docs/3.1.2/bitbake-user-manual/bitbake-user-manual.html)

- [Yocto Project Reference Manual 3.1.2 by Yocto](https://www.yoctoproject.org/docs/3.1.2/ref-manual/ref-manual.html)

- [Yocto Project Complete Documentation \(MegaManual\) Set 3.1.2 by Yocto](https://www.yoctoproject.org/docs/3.1.2/mega-manual/mega-manual.html)

- [A practical guide to BitBake by Harald Achitz](https://a4z.gitlab.io/docs/BitBake/guide.html)

## Quick Start

- Install `repo` command from Google if not available on your host system. Please follow [the official instructions](https://source.android.com/setup/develop#installing-repo) by Google.

- Then install a number of packages for BitBake (OE build tool) to work properly on your host system. BitBake itself depends on Python 3. Once you have Python 3 installed BitBake should be able to tell you most of the missing packages.

​      `For Ubuntu 18.04 (or newer) install python3-distutils package.`

- Essential Packages required for the Build Host (Debian/Ubuntu):

  ```bash
   $ sudo apt-get install gawk wget git-core diffstat unzip texinfo gcc-multilib \
     build-essential chrpath socat cpio python3 python3-pip python3-pexpect \
     xz-utils debianutils iputils-ping python3-git python3-jinja2 libegl1-mesa libsdl1.2-dev \
     pylint3 xterm 
  ```

  Detailed instructions for various distributions can be found in "[Required Packages for the Build Host](https://www.yoctoproject.org/docs/2.7.1/ref-manual/ref-manual.html#required-packages-for-the-build-host)" section in Yocto Project Reference Manual.

## Creating Workspace

This needs to be done every time you want a clean setup based on the latest layers.

```bash
mkdir riscv-cdac && cd riscv-cdac
repo init -u https://gitlab.com/cdachyd/mdp/meta-cdac -b master -m tools/manifests/cdac.xml
repo sync
```

## Creating a Working Branch

If you want to make modifications to existing layers then creating working branches in all repositories is advisable.

```bash
repo start work --all
```

### Updating Existing Workspace

If you want to pull in the latest changes in all layers.

```bash
cd riscv-cdac
repo sync
repo rebase
```

## Setting up Build Environment

This step has to be done after you modify your environment with toolchain you want to use otherwise wrong host tools might be available in the package build environment. For example, `gcc` from host system will be used for building `*-native` packages.

Here the script accepts 2 arguments that are MACHINE and IMAGE respectively. Currently **cdac-vega**  and **cdac-ooo** are supported as machines. And core-image-minimal is available as IMAGE. Default arguments are cdac-vega and core-image-minimal.  

```bash
source ./meta-cdac/setup.sh cdac-vega core-image-minimal
```

> You can verify and fix your host tools by checking symlinks in `$BUILDDIR/tmp-glibc/hosttools` directory.

### Configuring BitBake Parallel Number of Tasks/Jobs

There are 3 variables that control the number of parallel tasks/jobs BitBake will use: `BB_NUMBER_PARSE_THREADS`, `BB_NUMBER_THREADS` and `PARALLEL_MAKE`. The last two are the most important, and both are set to number of cores available on the system. You can set them in your `$BUILDDIR/conf/local.conf` or in your shell environment similar to how `MACHINE` is used (see next section). Example:

```bash
	PARALLEL_MAKE="-j 4" BB_NUMBER_THREADS=4 MACHINE=cdac-ooo bitbake demo-cdac-ooo-cli
```

Leaving defaults could cause high load averages, high memory usage, high IO wait and could make your system unresponsive due to resources overuse. The defaults should be changed based on your system configuration.

### Building for Out Of Order (cdac-ooo) board

1. ```bash
   MACHINE=cdac-ooo bitbake demo-cdac-ooo-cli
   ```

2. Generated output files are located at 

   ```bash
   cd ./tmp-glibc/deploy/images/cdac-ooo/demo-cdac-ooo-cli
   ```

3. Following files will be available

   ```
   Root File System: demo-cdac-ooo-cli-cdac-ooo.tar.gz
   Linux Kernel: Image
   U-Boot: u-boot-cdac-ooo.bin
   OpenSBI: fw_payload.bin 
   ```

4. To Build the SDK

   ```bash
   MACHINE=cdac-ooo bitbake demo-cdac-ooo-cli -c populate_sdk
   ```

   Generated SDK will be located at 

   ```
   cd ./tmp-glibc/deploy/sdk/
   ```

   

### Building for VEGA (cdac-vega) board

1. ```bash
   MACHINE=cdac-vega bitbake demo-cdac-vega-cli
   ```

2. Generated output files are located at

   ```bash
   cd tmp-glibc/deploy/images/cdac-vega/demo-cdac-vega-cli
   ```

3. Following files will be available

   ```
   Root file System: demo-cdac-vega-cli-cdac-vega.tar.gz
   Linux Kernel: Image
   OpenSBI: fw_payload.bin
   ```

4. To Build the SDK

   ```
   MACHINE=cdac-vega bitbake demo-cdac-vega-cli -c populate_sdk
   ```

   Generated SDK will be located at 

   ```bash
   cd ./tmp-glibc/deploy/sdk/
   ```



> Paths above are with reference to the native build directories   

#### NOTE:

1. Every image will have its own separate build directory including the downloads, tmp-glibc, conf and sstate-cache. Each build directory will be named in following method: build-{MACHINE}-{IMAGE}. MACHINE and IMAGE will be the arguments given while executing the setup script. 
2. As each image will have its own downloads and sstate-cache directories which can consume a lot of space. We can share these resources among multiple builds. For that make sure to change the DL_DIR and SSTATE_DIR in conf/local.conf of each image build directory with absolute paths.
