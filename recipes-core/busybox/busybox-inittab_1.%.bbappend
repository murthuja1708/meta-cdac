SUMMARY = "inittab configuration for BusyBox (CDAC-VEGA)"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:" 

SRC_URI =""

SRC_URI = "file://inittab"

S = "${WORKDIR}"


