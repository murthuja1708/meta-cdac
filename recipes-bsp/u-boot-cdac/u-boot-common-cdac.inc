HOMEPAGE = "http://www.denx.de/wiki/U-Boot/WebHome"
DESCRIPTION = "U-Boot, a boot loader for Embedded boards based on PowerPC, \
ARM, MIPS and several other processors, which can be installed in a boot \
ROM and used to initialize and test the hardware or to download and run \
application code."
SECTION = "bootloaders"
DEPENDS += "flex-native bison-native"


LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://Licenses/README;md5=30503fd321432fc713238f582193b78e"
PE = "1"

# We use the revision in order to avoid having to fetch it from the
# repo during parse

# v2020.07
#SRCREV = "AUTOINC"

#SRC_URI = "file://uboot.tar.gz;name=uboot \
#         "



SRC_URI = "git://git.denx.de/u-boot.git;name=u-boot \
	file://0001-Uboot-support-CDAC-SOC-Boots-on-BOTH-QEMU-on-Board.patch \    
	file://0002-Added-Eth-driver-to-U-boot.patch \      
	"
SRCREV_u-boot = "9cb895203a46654f7ee6dd95be5c8ab05e4dfbd3"
#SRC_URI[uboot.md5sum] = "c099c81ae1f0397c6a8ff2c492124604" 
S = "${WORKDIR}/git"
B = "${WORKDIR}/build"
do_configure[cleandirs] = "${B}"

#file://0001-Uboot-support-CDAC-SOC.-Boots-on-BOTH-QEMU-on-Board.patch 
