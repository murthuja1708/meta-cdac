require u-boot-common-cdac.inc
require u-boot-cdac.inc

DEPENDS += "bc-native dtc-native"

SRC_URI_append_freedom-u540 = " \
        file://mmc-boot.txt \
	file://0001-Uboot-support-CDAC-SOC-Boots-on-BOTH-QEMU-on-Board.patch \
	file://0002-Added-Eth-driver-to-U-boot.patch \	
        "

PROVIDES = "u-boot.bin"
