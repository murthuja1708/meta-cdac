DESCRIPTION = "minimalistic image"

include core-image-minimal.bb

IMAGE_INSTALL = "\
	${CORE_IMAGE_EXTRA_INSTALL} \
"

LICENSE = "MIT"

inherit extrausers

#USE_DEVFS ?= "0"
IMAGE_ROOTFS_SIZE ?= "8192"
EXTRA_USERS_PARAMS = "usermod -P root root;"
IMAGE_ROOTFS_EXTRA_SPACE_append = "${@bb.utils.contains("DISTRO_FEATURES", "systemd", " + 4096", "" ,d)}"
hostname_pn-base-files = "cdac"



EXTRA_IMAGE_FEATURES_append = " package-management"

IMAGE_INSTALL_append = " opkg ca-certificates openssl-bin"


